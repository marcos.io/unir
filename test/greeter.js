const Greeter = artifacts.require("Greeter");

contract('Greeter', async (account) => {

    beforeEach(async () => {
        //web3.personal.truff(web3.eth.accounts[0], 'Passw0rd');
    });

    it("Test greet", async() => {
        let instance = await Greeter.deployed();
        const retorno = await instance.greet('');
        assert.equal(retorno.logs[0].event, "GreeterEv");
        assert.equal(retorno.logs[0].args.message, '0x48656c6c6f2c20776f726c640000000000000000000000000000000000000000');
    });

    it("Test other greet", async() => {
        let instance = await Greeter.deployed();
        const retorno = await instance.greet('Hello');
        assert.notEmpty(retorno);
    });

    it ("Test kill", async() => {
        let instance = await Greeter.deployed();
        const retorno = await instance.kill();
        assert.notEmpty(retorno);
    })

})